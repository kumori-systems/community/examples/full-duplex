/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

internalServer = require('./internalServer') // Server attending requests from the internal duplex channel
publicServer = require('./publicServer')     // Server attending requests from the restapi server channel

try {
  // Starts the internal and public servers. The public server will receive REST
  // requests from users. The internal server is used to attend the requests sends
  // by other instances using the "internal" duplex channel
  internalServer.create()
  publicServer.create()
} catch (error) {
  console.log(`Error initializating component: ${error.message}`)
}