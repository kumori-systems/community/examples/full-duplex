/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

/*
 * The internal server attends requests comming from the duplex
 * "internal" channel. There are two possible request type:
 *
 *  * Get the value of an element stored in the instance store. If
 *    the requested element is not in the instance store, return a
 *    404 response with a "Not found" text.
 *  * Stores an element in the instance store.
 */

const fs = require('fs')
const express = require('express')
const bodyParser = require('body-parser');
const store = require('./store') // Store shated by the internal and public servers

// The server will listen in all IPs and using the port indicated in
// HTTP_INTERNAL_SERVER_PORT_ENV, which is set to the internal duplex
// channel port
const hostname = '0.0.0.0'
const port = process.env.HTTP_INTERNAL_SERVER_PORT_ENV

// All log lines written by the internal server will include this prefix
const logPrefix = "internalServer"

// createExpressApp function uses express to attend requests to the internal
// server REST API
function createExpressApp () {

  // Creates the express server and configures the bodyParser handler to
  // easily process POST requests bodies as text.
  var app = express()
  app.use(bodyParser.text({ type: "*/*" }));

  // Adds the handler to attend requests to get the value of an element stored
  // in this instance.
  app.get('/store/:element', (req, res) => {

    // Gets the key of the requested element
    const key = req.params.element
    console.log(`${logPrefix}. GET. Key: ${key}`)

    // Gets the value fot that key in this instance store. Recall that internal
    // and public servers share the same store
    const value = store.get(key)

    // If this instance store not holds the requested element, a 404 code is
    // returned with a "Not found" message. Otherwise, a 200 code is returned
    // with the current value for this element
    if (value !== undefined) {
      console.log(`${logPrefix}. GET. Response. Key: ${key} Value ${value}`)
      res.status(200).send(value)
    } else {
      console.log(`${logPrefix}. GET. Key: ${key}. Not found`)
      res.status(404).send("Not found")
    }
  })

  // Adds the handler to attend requests to store an element in this instance
  // local store.
  app.post('/store/:element', (req, res) => {

    // Gets the key and value for the element to be stored in this instance. The
    // value is stored in the requets body.
    const key = req.params.element
    const value = req.body
    console.log(`${logPrefix}. POST. Key: ${key} Value: ${value}`)

    // The value is saved in the store using the provided key
    store.set(key, value)

    // A 200 code is returned to configmr the sorage.
    res.sendStatus(200)
  })

  // Adds a handler to return a 404 error code to requests to any other paths
  app.use(function(req, res, next) {
    res.status(404).send('Not found')
  })

  // The express app is retturned
  return app
}

// Creates and returns the express app attending the requests received through the
// internal channel. The create method is the single element exported by this module.
exports.create = () => {
  const app = createExpressApp()
  app.listen(port, () => {
    console.log(`${logPrefix}. Internal server running at http://${hostname}:${port}/`)
  })
}
