/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

/*
 * The public server attends requests comming from the server
 * "restapi" channel. There are two possible request type:
 *
 *  * Get the value of an element stored in any instance store. If
 *    the requested element is not in the instance store, gets the
 *    list of existing replicas and sends a get request to them one
 *    by one using the "internal" duplex channel until it gets the
 *    value as a response. If none of them returns the value, a 404
 *    with a "Not found" text is returned.
 *  * Stores an element. If this instance already stores this element,
 *    the value is updated. Otherwise, gets the list of existint
 *    replicas and sends a get request of this value to them one by
 *    one. If any of them stores this element in its local store, the
 *    new value is sent to it. Otherwise, the element is stored in
 *    the instance store.
 */

const fs = require('fs')
const express = require('express')
const bodyParser = require('body-parser')
const store = require('./store')
const dns = require('dns')
const http = require('http')

// The server will listen in all IPs and using the port indicated in
// HTTP_PUBLIC_SERVER_PORT_ENV, which is set to the internal duplex
// channel port
const hostname = '0.0.0.0'
const port = process.env.HTTP_PUBLIC_SERVER_PORT_ENV

// All log lines written by the internal server will include this prefix
const logPrefix = "publicServer"

// createExpressApp function uses express to attend requests to the public
// server REST API
function createExpressApp () {

  // Creates the express server and configures the bodyParser handler to
  // easily process POST requests bodies as text.
  var app = express()
  app.use(bodyParser.text({ type: "*/*" }));

  // Adds the handler to attend requests to get the value of an element. If
  // it is not stored in this instance, the request is propagated to the
  // other replicas.
  app.get('/store/:element', async (req, res) => {

    // Gets the key of the requested element
    const key = req.params.element
    console.log(`${logPrefix}. GET. Key: ${key}`)

    // Gets the value fot that key in this instance store. Recall that internal
    // and public servers share the same store
    const value = store.get(key)

    if (value !== undefined) {

      // If the value is stored by this instance local store, the value is returned
      console.log(`${logPrefix}. GET. Response. Key: ${key} Value: ${value}`)
      res.status(200).send(value)

    } else {
      try {

        // If the value is not stored in the instance local store, ask to the other
        // partners (replicas) if they have that element stored and returns the value
        // if so.
        const result = await askPartners(key)
        console.log(`${logPrefix}. GET. Response. Key: ${key} Value: ${result.value}`)
        res.status(200).send(result.value)

      } catch(error) {

        if (error.message.localeCompare("Not found") == 0) {

          // If this element is not stored in any replica, returns a 404 code with
          // the "Not found" text
          console.log(`${logPrefix}. GET. Response. Key: ${key}. Not found`)
          res.status(404).send(error.message)

        } else {

          // If any other error shows up, a 500 code is returned with the error message
          console.log(`${logPrefix}. GET. Response. Key: ${key} Error: ${error.message}`)
          res.status(500).send(error.message)

        }
      }
    }
  })

  // Adds the handler to attend requests to store an element. If this element is already
  // stored in any replica, the value is updated in that replica.
  app.post('/store/:element', async (req, res) => {

    // Gets the key and value for the element to be stored. The
    // value is stored in the requets body.
    const key = req.params.element
    const value = req.body
    console.log(`${logPrefix}. POST. Key: ${key} Value: ${value}`)

    if (store.has(key)) {

      // If this instance already stores this element, the value is updated and a
      // 200 code is returned.
      store.set(key, value)
      console.log(`${logPrefix}. POST. Stored. Key: ${key} Value: ${value}`)
      res.sendStatus(200)

    } else {
      try {

        // If this element is stored in any other replica, the new value is sent
        // to this replica using the "internal" duplex channel and a 200 code is
        // returned.
        const result = await askPartners(key)
        await propagatePost(result.address.name, result.address.port, key, value)
        console.log(`${logPrefix}. POST. Stored. Key: ${key} Value: ${value}`)
        res.sendStatus(200)

      } catch(error) {

        if (error.message.localeCompare("Not found") == 0) {

          // If the element is not stored in any replica then it is stored in this
          // one local store and a 200 code is returned
          store.set(key, value)
          console.log(`${logPrefix}. POST. Stored. Key: ${key} Value: ${value}`)
          res.sendStatus(200)

        } else {

          // If any other error appears, a 500 code is returned with the error message
          console.log(`${logPrefix}. POST. Response. Key: ${key} Error: ${error.message}`)
          res.status(500).send(error.Message)

        }
      }
    }
  })

  // Adds the handler to respond to health requests
  app.get('/health', (req, res) => {
    console.log('Health request')
    res.send('OK')
  })

  // Adds a handler to return a 404 error code to requests to any other paths
  app.use(function(req, res, next) {
    res.status(404).send('Not found')
  })

  return app
}

// This method is asynchronous and returns a promise. This promise is resolved
// with the requested element value if it is stored in any frontend replica. The
// promise is rejected with a "Not found" error if the requested element is not
// stored in any replica.
async function askPartners (key) {

  // Returns a promise which will be resolved with the requested element value or
  // rejected with a "Not found" error.
  return new Promise((resolve, reject) => {

    // The frontend replicas IPs and the port of their internal server are stored
    // as a SRV records in the cluster internal DNS after a name composed using
    // the channel name, the role name, the service internal name and a suffix. For
    // example: "internal.frontend000.kd-055010-033c3df0.kumori.dep.cluster.local".
    // In any instance, everything after the channel name is included in
    // /etc/resolve.conf as a search subdomain which allows any application to get
    // the SRV values just by resolving the channel name ("internal" in this case).
    // However, this only works if the application resolution library calls the O.S.
    // libuv getaddrinfo. Unfortunately, the NodeJS dns.resolveSrv is not using the
    // libuv library, as explained in the following link:
    //
    // https://nodejs.org/docs/latest-v14.x/api/dns.html#dns_implementation_considerations
    //
    // To overcome this issue, the Kumori platform adds to each instance a
    // KUMORI_ROLE_SUBDOMAIN containing the subdomain after the channel name
    // ("frontend000.kd-055010-033c3df0.kumori.dep.cluster.local") in the previous
    // example. If your DNS resolver is not using libuv (as in this case) you must
    // concatenate this subdomain to the channel name to get the SRV records after
    // a duplex channel.
    let hostname = `internal.${process.env.KUMORI_ROLE_SUBDOMAIN}`

    // Resolves the channel name to get the frontend instances IPs and the port of
    // their internal server.
    dns.resolveSrv(hostname, async (err, addresses) => {

      // If any problem appears resolving the channel name, the promise is rejected
      // including the error
      if (err) {
        reject(err)
        return
      }

      // Requests the element to each instance until one of them returns the value.
      for (let address of addresses) {
        try {

          // Requests the element to the instance
          const value = await propagateGet(address.name, address.port, key)

          // If this instance stores the element, the value is returned with the instance
          // internal server address. Note that propagateGet returns an error if
          // the element is not stored in that instance
          resolve({
            value: value,
            address: address
          })

          return

        } catch(error) {

          // The propagateGet will throw an error if the element is not found in
          // the requestes address. If that's the case, we try with the following
          // address
          console.log(`${logPrefix}. GET. Response. Key: ${key}. Not found in ${address.name}:${address.port}`)
        }
      }

      // If the element is not found in any replica, a "Not found" error is returned.
      reject(new Error("Not found"))
    })
  })
}

// Requests a given element to a given internal server represented by a host and
// a port. This function is anynchronous and uses promises. Hence, a promise is
// returned. If the element is found in the given instance, the promise is resolved
// with the value. Otherwise, the promise is rejected with a "Not found" error
function propagateGet (host, port, key) {

  // Returns a promise. It will be resolved with the element value or rejected with
  // a "Not found" error
  return new Promise((resolve, reject) => {

    // Performs a GET HTTP request using the /store/<key> subpath
    http.get(`http://${host}:${port}/store/${key}`, (res) => {

      // Reads the response as an utf8 text. The response is stored in rawData
      res.setEncoding('utf8')
      let rawData = ''

      // If a 404 code is returned, then the promise is rejected with a
      // "Not found" error
      if (res.statusCode == 404) {
        reject(new Error("Not found"))
        return
      }

      // Each chunk is concatenated in rawData
      res.on('data', (chunk) => { rawData += chunk; })

      // Once the response is complete, the promise is resolved with it.
      res.on('end', () => {
        resolve(rawData)
      })

    }).on('error', (error) => {

      // If any unexpected error appears, the promise is rejected with that error
      reject(error)
    })
  })
}

// Stores a given element in a given internal server represented by a host and
// a port. This function is anynchronous and uses promises. Hence, a promise is
// returned. If the element is stored in the given instance, the promise is resolved
// with the response (usually empty). Otherwise, the promise is rejected with the
// error found
function propagatePost (host, port, key, value) {

  // Returns a promise. It will be resolved if the element is stored or rejected if
  // any error shows up
  return new Promise((resolve, reject) => {

    // The element is obtainer using a POST request to /store/<key>. The value is
    // included as text in the request body.
    const options = {
      hostname: host,
      port: port,
      path: `/store/${key}`,
      method: 'POST',
      headers: {
        'Content-Type': 'text/plain',
        'Content-Length': Buffer.byteLength(value)
      }
    }

    // Performs the POST call to /store/<key>
    const req = http.request(options, (res) => {

      // Reads the response as an utf8 text. The response is stored in rawData.
      // It is usually empty.
      res.setEncoding('utf8')
      let rawData = ''

      // If a 404 code is returned, then the promise is rejected with a
      // "Not found" error
      if (res.statusCode == 404) {
        reject(new Error("Not found"))
        return
      }

      // Each chunk is concatenated in rawData
      res.on('data', (chunk) => { rawData += chunk; })

      // Once the response is complete, the promise is resolved with it.
      res.on('end', () => {
        resolve(rawData)
      })
    })

    req.on('error', (error) => {
      // If any unexpected error appears, the promise is rejected with that error
      reject(error)
    })

    // The element value is written as the request body text.
    req.write(value)
    req.end()
  })
}

// Creates and returns the express app attending the requests received through the
// restapi channel. The create method is the single element exported by this module.
exports.create = () => {
  const app = createExpressApp()
  app.listen(port, () => {
    console.log(`${logPrefix}. Public server running at http://${hostname}:${port}/`)
  })
}
