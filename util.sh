#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="fullduplexinb"
DEPLOYNAME="fullduplexdep"
DOMAIN="fullduplexdomain"
SERVICEURL="fullduplex-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "FullDuplex service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

# Test the hello-world service
'test')
  echo; echo Storing several values; echo
  curl -d 'myvalue1' -H "Content-Type: text/plain" -X POST https://${SERVICEURL}/store/myelement1; echo myelement1/myvalue1
  curl -d 'myvalue2' -H "Content-Type: text/plain" -X POST https://${SERVICEURL}/store/myelement2; echo myelement2/myvalue2
  curl -d 'myvalue3' -H "Content-Type: text/plain" -X POST https://${SERVICEURL}/store/myelement3; echo myelement3/myvalue3
  curl -d 'myvalue4' -H "Content-Type: text/plain" -X POST https://${SERVICEURL}/store/myelement4; echo myelement4/myvalue4
  echo; echo Retrieving the stored values; echo
  curl https://${SERVICEURL}/store/myelement1; echo
  curl https://${SERVICEURL}/store/myelement2; echo
  curl https://${SERVICEURL}/store/myelement3; echo
  curl https://${SERVICEURL}/store/myelement4; echo
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

# Undeploy all
'undeploy-all')
  $0 unlink
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac