package service

import (
  f ".../frontend:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    config: {
      parameter: {}
      resource: {}
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {}
          resource: {}
          resilience: description.config.resilience
        }
      }
    }

    srv: {
      server: restapi: { protocol: "http", port: 80 }
    }

    connect: {
      // Outside -> FrontEnd (LB connector)
      cinbound: {
        as: "lb"
        from: self: "restapi"
        to: frontend: "restapi": _
      }
      // Frontend -> Frontend (Full Connector)
      cinternal: {
        as: "full"
        from: {}
        to: frontend: "internal": _
      }
    }
  }
}
