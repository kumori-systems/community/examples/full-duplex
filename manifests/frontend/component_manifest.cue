package component

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: restapi: { protocol: "http", port: 8080 }
      duplex: internal: { protocol: "http", port: 8081 }
    }

    config: {
      parameter: {}
      resource: {}
    }

    size: {
      bandwidth: { size: 10, unit: "M" }
    }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 120000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: {
      frontend: {
        image: {
          hub: { name: "", secret: "" }
          tag: "kumoripublic/examples-full-duplex-frontend:v1.0.8"
        }
        mapping: {
          env: {
            HTTP_PUBLIC_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
            HTTP_INTERNAL_SERVER_PORT_ENV: value: "\(srv.duplex.internal.port)"
          }
        }
        size: {
          memory: { size: 100, unit: "M" }
          mincpu: 100
          cpu: { size: 200, unit: "m" }
        }
      }
    }
  }
}
